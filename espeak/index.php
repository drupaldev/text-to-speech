<?php
// change following setting to a cache folder could increase performance
$base_dir = '/tmp';

// get input parameters
$text = $_GET['text'];

if (isset($_GET['voice'])) {
  $voice = $_GET['voice'];
} else {
  $voice = 'en';
}

if (isset($_GET['speedDelta'])) {
  $speed_delta = $_GET['speedDelta'];
} else {
  $speed_delta = 0;
}
$speed = (int)(130 + 175 * $speed_delta / 100);

if (isset($_GET['pitchDelta'])) {
  $pitch_delta = $_GET['pitchDelta'];
} else {
  $pitch_delta = 0;
}
$pitch = (int)(50 + $pitch_delta / 2);

if (isset($_GET['volumeDelta'])) {
  $volume_delta = $_GET['volumeDelta'];
} else {
  $volume_delta = 0;
}
$volume = (int)(100 + $volume_delta);

$filename = md5($text) . '.mp3';
$filepath = $base_dir . '/v' . $voice . 's' . $speed . 'p' . $pitch .
    'a' . $volume . 't' . $filename;
setlocale(LC_CTYPE, "en_US.UTF-8");
$text = escapeshellarg($text);
if (!file_exists($filepath)) {
  $cmd = "espeak -s $speed -p $pitch -a $volume --stdout $text |\lame --preset voice -q 9 --vbr-new - $filepath";

// $cmd;

  exec($cmd);
}

header('Content-Type: audio/mpeg');
header('Content-Length: ' . filesize($filepath));
readfile($filepath);
?>
