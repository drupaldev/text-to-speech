<?php 

namespace Drupal\webspeech\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class WebSpeechForm extends FormBase {

 public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
  global $base_url;
  $user_permissions = $this->get_user_permissions();

  $user_roles = \Drupal::currentUser()->getRoles();

  
  if(in_array('administrator', $user_roles) || in_array('access webspeech',$user_permissions))
    $per = true;
  else $per = false;

  if ($per) {  
	  
    $form['sideSprButton'] = array(
       '#type' => 'button',
       '#value' => t('Read Content'),
       '#attributes' => array('onclick' => "javascript:sideSpr(this);"),
    );  
    $form['sideStopButton'] = array(
       '#name' => 'clear',
       '#type' => 'button',
       '#value' => t('Stop'),
       '#attributes' => array('onclick' => "javascript:sideStop();"),
     );
    return $form;
  }
  
  else return NULL;
  }

  /**   * {@inheritdoc}   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    

  }

  /**   * {@inheritdoc}   */
  public function getFormId() {
    return 'webspeech_form';
  }

  public function get_user_permissions()
  {

    $user = \Drupal::currentUser();
    $user_roles = $user->getRoles();
    $roles_permissions = user_role_permissions($user_roles);

$final_array = array();
foreach ($roles_permissions as $role_key => $permissions) {
  foreach ($permissions as $permission) {
    $final_array[] = $permission;
  }
}

return $final_array;
  }

}
