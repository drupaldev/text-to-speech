<?php 
namespace Drupal\webspeech\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ConfigForm extends ConfigFormBase {

 public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {

 $user_roles = \Drupal::currentUser()->getRoles();

  if(in_array('administrator', $user_roles))
    $per = true;
  else $per = false;

  if ($per) {  

    $form['server'] = array(
    '#type' => 'textfield',
    '#title' => t('WebSpeech Server URL'),
    '#default_value' => \Drupal::config('webspeech.settings')->get('webspeech_server_url'),
       // '#prefix' => '<div>'.var_dump($user_permissions).'</div>',
    );

    $form['content_id'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Element Id'),
    '#description' => t('Enter the id of div which contains text to be read.<br> e.g If your content is <div id="read">you content here</div>, Enter "read" in this field.'),
    '#default_value' => \Drupal::config('webspeech.settings')->get('webspeech_content_id'),
    );

    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

   
    return $form;
  }
  
  else return NULL;
  }

  /**   * {@inheritdoc}   */

   public function submitForm(array &$form, FormStateInterface $form_state) {


    \Drupal::configFactory()->getEditable('webspeech.settings')
            ->set('webspeech_server_url', $form_state->getValue('server'))
            ->save();

          \Drupal::configFactory()->getEditable('webspeech.settings')
            ->set('webspeech_content_id', $form_state->getValue('content_id'))
            ->save();

    

    \Drupal::messenger()->addMessage(t('Webspeech Settings saved successfully.'));
  }

  /**   * {@inheritdoc}   */
  public function getFormId() {
    return 'webspeech_config';
  }

  /**   * Gets the configuration names that will be editable.   *   * @return array   *   An array of configuration object names that are editable if called in   *   conjunction with the trait's config() method.   */

  protected function getEditableConfigNames() {
    return ['webspeech.settings'];
  }

}
