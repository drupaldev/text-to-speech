<?php
/**
 * @file
 * Contains \Drupal\webspeech\Plugin\Block\WebSpeechBlock.
 */

namespace Drupal\webspeech\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'webspeech' block.
 *
 * @Block(
 *   id = "webspeech_block",
 *   admin_label = @Translation("WebSpeech block"),
 *   category = @Translation("Custom webspeech block")
 * )
 */
class WebSpeechBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
  global $base_url;
    //$form = \Drupal::formBuilder()->getForm('Drupal\webspeech\Form\WebSpeechForm');
    //return $form;
   $library_folder = $base_url . '/libraries/WebSpeech';
    $webspeech_server_url = \Drupal::config('webspeech.settings')->get('webspeech_server_url');
    $webspeech_voice = \Drupal::config('webspeech.settings')->get('webspeech_voice');
    $webspeech_content_id = \Drupal::config('webspeech.settings')->get('webspeech_content_id');


    $webspeech_config = \Drupal::config('webspeech.settings');

         $build = [
          '#type' => 'item',
          '#markup' => '<span class="webspeech_items"></span>',
          '#prefix' => '<div class="webspeech_container">',
          '#suffix' => '</div>',
          '#attached' => [
            'library' => 'webspeech/global-styling',
            'drupalSettings' => [
              'wsdefault' => [                 
                 'webspeech_server_url' => $webspeech_server_url,
                 'webspeech_voice' => $webspeech_voice,
                 'webspeech_content_id' => $webspeech_content_id,
                 'library_path' => $library_folder,            
              ],            
            ],
           ],
          ];

          return $build; 
   }
}