(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.wsDefault = {
    attach: function (context, settings) {   	

      var webspeech_voice = drupalSettings.wsdefault.webspeech_voice;
      var webspeech_server_url = drupalSettings.wsdefault.webspeech_server_url;
      var webspeech_content_id = drupalSettings.wsdefault.webspeech_content_id;
      var webspeech_library_path = drupalSettings.wsdefault.library_path;

  
      
      soundManager.url = webspeech_library_path+'/soundmanager2';
      WebSpeech.server = webspeech_server_url;
      WebSpeech.setVoice(webspeech_voice);    


      $(".webspeech_items").html('<button id="sideSprButton" >Read Content</button><button id="sideStopButton" >Stop</button>');

      $('#sideSprButton').click(function () {
      $(this).next('ul').toggle('show');

       if (typeof WebSpeech === 'undefined') {       	
          return;
        }

        var value = this.innerHTML;
        if (value === 'Read Content') {
          WebSpeech.speakHtml(webspeech_content_id);
          this.innerHTML = 'Pause';
          WebSpeech.onfinish = function () {
          document.getElementById('sideSprButton').innerHTML = 'Read Content';
          }
        }
        else if (value === 'Pause') {
          WebSpeech.pauseHtml();
          this.innerHTML = 'Resume';
        }
        else if (value === 'Resume') {
          WebSpeech.resumeHtml();
          this.innerHTML = 'Pause';
        }
    });

      $('#sideStopButton').click(function () {
           if (typeof WebSpeech !== 'undefined') {
             WebSpeech.stopHtml();
             document.getElementById('sideSprButton').innerHTML = 'Read Content';
           }
      });


    }



  }

  function sideSpr(elem) {
        if (typeof WebSpeech === 'undefined') {
          return;
        }

        var value = elem.innerHTML;
        if (value === 'Read Content') {
          WebSpeech.speakHtml(webspeech_content_id);
          elem.innerHTML = 'Pause';
          WebSpeech.onfinish = function () {
          document.getElementById('sideSprButton').innerHTML = 'Read Content';
          }
        }
        else if (value === 'Pause') {
          WebSpeech.pauseHtml();
          elem.innerHTML = 'Resume';
        }
        else if (value === 'Resume') {
          WebSpeech.resumeHtml();
          elem.innerHTML = 'Pause';
        }
      }

      function sideStop() {
        if (typeof WebSpeech !== 'undefined') {
          WebSpeech.stopHtml();
          document.getElementById('sideSprButton').innerHTML = 'Read Content';
       }
      }
})(jQuery, Drupal, drupalSettings);